from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class Project(models.Model):
    name= models.CharField(max_length=200)
    description= models.TextField()
    owner= models.ForeignKey(
        USER_MODEL,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True
    )

def __str__(self):
    return self.name
# this __str__ method is a way to define
# how an instance of a class should be
# represented as a string. When you print
# an object of this class or convert it
# to a string, the string returned will
# be the value of its name attribute.
